There is a separate Bitbucket project for the c++ implementation. It is at
https://bitbucket.org/energistics/etp-cpp

The etp-cpp project contains a full sample implementation of ETP in c++. It contains instructions for how to build the sample using make. The sample implementation uses Node.js and CMake in the build process and uses the Boost libraries and Avro in the running of the application, so there are dependencies on those. Everything is described in the Bitbucket project.