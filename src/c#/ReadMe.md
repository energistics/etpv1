The c# proxy classes for ETP are delivered as a NuGet package.
The home page for the package is here.
https://www.nuget.org/packages/ETP

Install from a NuGet Package Manager command prompt with the following:
PM> Install-Package ETP -Pre

A more complete example of a simple client and server in c# can
be cloned from the etp samples git repository.

https://bitbucket.org/energistics/etpsamples