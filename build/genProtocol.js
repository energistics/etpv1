/**
 *
 * @type {*}
 */
var path = require('path');
var fs = require('fs');
argv = require('optimist')
    .default(
    {
        schemas: 'src/Schemas',
        outputProtocolFile: "src/Schemas/etp.avpr",
        outputJavaScriptFile: "src/javascript/lib/etp-schemas.js",
        verbose: false,
		p: false,
		s: false
    }
    ).argv;

/* Sort into dependency order. */
function toposort(nodes, edges) {
  var cursor = nodes.length
    , sorted = new Array(cursor)
    , visited = {}
    , i = cursor

  while (i--) {
    if (!visited[i]) visit(nodes[i], i, [])
  }

  return sorted

  function visit(node, i, predecessors) {
    if(predecessors.indexOf(node) >= 0) {
      throw new Error('Cyclic dependency: '+JSON.stringify(node))
    }

    if (visited[i]) return;
    visited[i] = true

    // outgoing edges
    var outgoing = edges.filter(function(edge){
      return edge[0] === node
    })
    if (i = outgoing.length) {
      var preds = predecessors.concat(node)
      do {
        var child = outgoing[--i][1]
        visit(child, nodes.indexOf(child), preds)
      } while (i)
    }

    sorted[--cursor] = node
  }
}	
	
/**
 * @function isComplex
 * @param name
 * @returns {boolean}
 */
function isComplex(name) {
    return ['boolean', 'int', 'long', 'double', 'float', 'string', 'fixed', 'bytes', 'null', 'enum'].indexOf(name) < 0;
}

function typeList(schema) {
    switch (typeof schema) {
        case "string":
            if (isComplex(schema))
                this.depends.push(schema);
            break;
        case "object":
            if (schema.fields)
                schema.fields.map(typeList, this);
            else if (schema instanceof Array)
                schema.map(typeList, this);
            else if (schema.type == 'array')
                typeList.bind(this)(schema.items);
            else if (schema.type == 'map')
                typeList.bind(this)(schema.values);
            else if (schema.type)
                typeList.bind(this)(schema.type);
            else
                throw ("Unknown schema type in typeList()");
            break;
    }

    return this;
}


var walk = function (dir, done) {
    var results = [];
    fs.readdir(dir, function (err, list) {
        if (err) return done(err);
        var pending = list.length;
        if (!pending) return done(null, results);
        list.forEach(function (file) {
            file = dir + '/' + file;
            fs.stat(file, function (err, stat) {
                if (stat && stat.isDirectory()) {
                    walk(file, function (err, res) {
                        results = results.concat(res);
                        if (!--pending) done(null, results);
                    });
                } else {
                    results.push(file);
                    if (!--pending) done(null, results);
                }
            });
        });
    });
};

var schemas = [];
var schemaFileNames = []; //fs.readdirSync(argv.schemas);

var protocol = {
    namespace: "Energistics",
    protocol: "Etp",
    version: "1.2",
    types: []
};

walk(argv.schemas, function (err, results) {
    if (err) throw err;
    if (argv.verbose)
        console.log(results);
    schemaFileNames = results;

    for (var i = 0; i < schemaFileNames.length; ++i)
        if (schemaFileNames[i].match(/.*\.avsc/)) {
            if(argv.verbose)
                console.log(schemaFileNames[i]);
            var schema = JSON.parse(fs.readFileSync(schemaFileNames[i], 'utf8'));
            schema.fullName = schema.namespace + "." + schema.name;
            schemas[schema.fullName] = schema;
            schemas.push(schema);
        }

    for (var i = 0; i < schemas.length; i++) {
        if(argv.verbose)
            console.log(schemas[i].fullName);
        schemas[i].depends = [];
        typeList.bind(schemas[i])(schemas[i]);
        if(argv.verbose)
            console.log(schemas[i].depends);
    }
	
	// Build adjacency list of dependent nodes.
	var edges= [];
    for (var i = 0; i < schemas.length; i++) {
		for (var j=0; j < schemas[i].depends.length; j++) {
			edges.push([schemas[schemas[i].depends[j]], schemas[i]]);
		}
    }
	
	// Sort into dependency order.
	schemas = toposort(schemas, edges);
	

    schemas.map(function (schema) {
        if(argv.verbose)
            console.log(schema.name);
        protocol.types.push(schema);
        var parts=[];
        if(schema.namespace) {
            parts=schema.namespace.split(".");
        }
        //parts.push(schema.name);
        var currentObject = protocol;
        while (parts.length > 0)
        {
            if(!currentObject[parts[0]])
            currentObject[parts[0]]={};
            currentObject = currentObject[parts.shift()];
        }
        currentObject[schema.name]=schema;
		if (argv.verbose)
			console.dir(protocol);
    });

	console.dir(argv);
	
    // Create as .avpr
	if (argv.p) {
		console.log("Creating protocol file: " + argv.outputProtocolFile);
		fdw = fs.openSync(argv.outputProtocolFile, 'w');
		fs.writeSync(fdw, JSON.stringify(protocol, null, 2));
		fs.close(fdw);
	}

    // Create as javascript object for Node
	if (argv.s) {
		console.log("Creating javascript schemas: " + argv.outputJavaScriptFile);
		fdw = fs.openSync(argv.outputJavaScriptFile, 'w');
		fs.writeSync(fdw, "var RalfSchemas = JSON.parse('"+ JSON.stringify(protocol) + "');\n\nmodule.exports=RalfSchemas;\n");
		fs.close(fdw);
	}
});
