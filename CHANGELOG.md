# Changelog
All notable changes to the schemas will be documented in this file. The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Dev 26 [2021-09-09]
### Added
- "Unordered" symbol to IndexDirection enumerator

### Corrected
- Cardinality on the "reason" field of ChannelsClosed, SubscriptionStopped, SubscriptionEnded and PartSubscriptionEnded

## Dev 25 [2021-09-01]
### Corrected
- Type to string in field "reason" of PartSubscriptionEnded 

## Dev 24 [2021-09-01]
### Added
- String field "reason" to ChannelsClosed, SubscriptionStopped, SubscriptionEnded and PartSubscriptionEnded

### Changed
- Deprecated protocol symbols in Protocol enum renamed to RESERVED_##
- Field name "uris" to "success" in GrowingObject.PutGrowingObjectsHeaderResponse

## Dev 23 [2021-08-19]
### Added
- customData symbol to AnyLogicalArrayType enumerator 
- joinedSubscription and unjoinedSubscription symbols to ObjectChangeKind enumerator
- includeAllChannelSecondaryIndexes to ChannelDataFrame.GetFrameMetadata as boolean with default of false
- indexes to ChannelDataFrame.GetFrameResponseHeader as array of IndexMetadataRecord
- earliestRetainedChangeTime to OpenSession and RequestSession as long

## Dev 22 [2021-05-26 to 2021-06-09]
### Added
- AnyLogicalArrayType enumerator with requested values
- Added the following in DataArrayMetadata
  - referredSubarrayDimensions as an optional array of long
  - logicalArrayType as a mandatory AnyLogicalArrayType
  - customData as an optional map of DataValues
  - storeLastWrite as a mandatory long
  - storeCreated as a mandatory long
- PutDataArraysType added customData as an optional map of DataValues
- storeLastWrite as a mandatory long in Dataspace 
- storeCreated as a mandatory long in Dataspace 

### Changed
- Renamed protocols in Protocols enum to:
	- DEPRECATEDDataArrayNotification
	- DEPRECATEDChannelStreamingQuery
	- DEPRECATEDChannelDataFrameQuery
	- DEPRECATEDStoreNotificationQuery
	- DEPRECATEDGrowingObjectNotificationQuery
	- DEPRECATEDDataArrayQuery
	- DEPRECATEDDataArrayNotificationQuery
	- DEPRECATEDChannelView
- Renamed GetDataspaces.lastChangedFilter to storeLastWriteFilter
- Role in Authorize and AuthorizeResponse to "client,server"
- Error 10 EEXPIRED_TOKEN to EAUTHORIZATION_EXPIRED
- RequestSession.endpointCapabilities to 0..* and default of EmptyMap
- RequestSession.supportedCompression to 0..* and default of EmptyArray
- Modified a bunch of multiplicity n's to *'s
- RequestSession.supportedCompression look like ServerCapabilities.supportedCompression (i.e., made it optional with a default of [])
- Renamed FrameChannelMetadataRecord.dataType to dataKind
- Renamed arrayType to transportArrayType in DataArrayMetadata

### Removed
- Error 4001 EUNSUPPORTED_OBJECT
- "WitsmlSoap" symbol from Protocols enumerator
- LastChanged in Dataspace 

## Dev 21 [2021-04-16]
### Added
- added uom, depthDatum, indexPropertyKindUri, and filterable to IndexMetadataRecord
- added TrueVerticalDepth, Temperature and Pressure to ChannelIndexKind
- added DateTime, ElapsedTime, MeasuredDepth, PassIndexedDepth, and TrueVerticalDepth to ChannelDataKind
- added depthDatum to ChannelMetadataRecord
- added uom, depthDatum, attributePropertyKindUri, and axisVectorLengths to AttributeMetadataRecord
- added supplementalAuthorization as an optional map of string
- added serverAuthorizationRequired to RequestSession
- added EAUTHORIZATION_REQUIRED and EAUTHORIZATION_EXPIRING 
- added RequestSessionTimeoutPeriod and SessionEstablishmentTimeoutPeriod to EndpointCapabilityKind
- added SupportsSecondaryIndexFiltering to ProtocolCapabilityKind
- added MaxSecondaryIndexCount to DataObjectCapabilityKind
- added depthDatum
- ArrayOfBytes record [plural because the primitive type in Avro is bytes, not byte]
- added arrayOfBytes to DataValue
- added includeAllChannelSecondaryIndexes and requestedSecondaryIntervals to GetFrame requestedSecondaryIntervals has nullable and default=[] on the same property
- added requestedSecondaryIntervals to ChannelRangeInfo
- added index to PartsMetadataInfo
- PassDirection enumerator with symbols "Up", "HoldingSteady" and "Down"
- ArrayOfBytes record

### Changed
- Changed PassIndexedDepth.direction to use PassDirection
- Renamed ChannelIndexKind.Time to DateTime and Depth to MeasuredDepth
- Sorted ChannelIndexKind
- Renamed DataValueType to ChannelDataKind
- Moved ChannelDataKind to the ChannelData namespace
- Renamed dataType to dataKind in ChannelMetadataRecord it already uses the renamed ChannelDataKind
- renamed dataType to dataKind it already uses the renamed ChannelDataKind
- renamed measureClass to channelPropertyKindUri on FrameChannelMetadataRecord
- renamed RenewSecurityToken to Authorize
- renamed token to authorization
- renamed RenewSecurityTokenResponse to AuthorizeResponse
- renamed renewed to success
- Sorted ChannelIndexKind as requested (didn't follow the comment about DataValueType)
- Sorted ChannelMetadataRecord.axisVectorLengths to above attributeMetadata (believing that attributeMetadata and customData should be the last entries)
- Moved ChannelDataKind into the file Datatypes\ChannelData\ChannelDataKind.avsc
- Made DateTime the default for IndexMetadataRecord.indexKind
              
### Removed
- removed dataObjectType from Resource
- removed dataObjectType from DeletedResource
- Removed TrueVerticalDepth, typeArrayOfInt, typeArrayOfLong, typeArrayOfFloat, typeArrayOfDouble, and typeArrayOfString from ChannelDataKind
- removed indexKind and indexInterval from PartsMetadataInfo
- removed token from RequestSession
- removed token from OpenSession
- removed description
- The null union from ChannelRangeInfo.secondaryIntervals
- The null union from GetFrame.requestedSecondaryIntervals

## Dev 20 [2021-03-24]
### Added
- “PassIndexedDepth” symbol to the ChannelIndexKind enum
- PassIndexedDepth record
- “Energistics.Etp.v12.Datatypes.ChannelData.PassIndexedDepth” symbol to the IndexValue enum

## Dev 19 [2021-03-23]
### Added
- relationshipKind to SupportedType per the thread in Basecamp

### Changed
- dataObjectCapabilities to map of DataValue
- moved customData to the end of the list for:
  - FrameChannelMetadataRecord
  - ChannelMetadataRecord
  - Resource

## Dev 18 Post RC2 [2021-03-17]
### Added
- storeCreated and activeStatus to Resource
- Renewed and challenges to Core.RenewSecurityTokenResponse
- token to Core.RequestSession
- token to Core.OpenSession
- GrowingObject.GetChangeAnnotations with sinceChangeTime as a long uris as a map of string [says array one place and map the other] and latestOnly as a boolean with a default of false
- GrowingObject.GetChangeAnnotationsResponse with changes as a map of ChangeResponseInfo
- DataObjectCapabilityKind enumeration
- Symbols to EndpointCapabilityKind

### Changed
- Energistics.Etp.v12.Datatypes.ChannelData.ChannelChangeAnnotation to Energistics.Etp.v12.Datatypes.Object.ChangeAnnotation
- Moved Energistics.Etp.v12.Datatypes.ChannelData.ChannelChangeResponseInfo to Energistics.Etp.v12.Datatypes.Object.ChangeResponseInfo
- Renamed Datatypes.ChannelData.ChannelMetadataRecord.measureClass To channelClassUri (left the Description alone)
- Renamed ChangeResponseInfo.channelChanges to ...
- Modified changes to map of array of Energistics.Etp.v12.Datatypes.Object.ChangeAnnotation 
- Modified ChannelSubscribe.GetChangeAnnotationsResponse.channels to ChangeResponseInfo
- Modified ChannelDataLoad.ReplaceRangeResponse.channelChangeTime to be a plain long (no map)
- Names of symbols in the ProtocolCapabilityKind enumrator

### Removed
- ChannelData.Role [checked to make sure nothing was using it first]
- startTime from SubscriptionInfo

## Dev 17 [2020-10-23]
### Changed
- Changed Organizational to Primary in Energistics.Etp.v12.Datatypes.Object.RelationshipKind
- Changed Contextual to Secondary in Energistics.Etp.v12.Datatypes.Object.RelationshipKind
- Changed includeContextualTargets TO includeSecondaryTargets in Energistics.Etp.v12.Datatypes.Object.ContextInfo
- Changed includeContextualSources TO includeSecondarySources in Energistics.Etp.v12.Datatypes.Object.ContextInfo

## Dev 16 [2020-10-17]
### Changed
- Modified OpenChannelInfo object
- Modified ContextInfo object
- Modified Edge object
- Modified OpenChannels object

### Removed
- DeleteNotificationRetentionPeriod from ProtocolCapabilityKind

## Dev 15 [2020-09-26]
### Added
- RelationshipKind enumarator in Object package

### Changed
- Three datatypes for customData, so not much to see here.

## Dev 14 [2020-08-13]
### Added
- DeleteRetentionTime as a long to ProtocolCapabilityKind
- relationshipKind to ContextInfo as a string (can't remember if this is an enum or something other than a simple string)
- GetDeletedResources to Discovery protocol with these fields:
  - dataspaceUri as a mandatory string
  - deleteTimeFilter as an optional long
  - Mandatory array of string with a default of empty array
- GetDeletedResourcesResponse as a mulipart message with an optional array of DeletedResources
- DeletedResource type in Object package with:
  - uri as a mandatory string
  - dataObjectType as a mandatory string
  - deletedTime as a mandatory long
  - customData as an optional map of strings
- Edge type in Object package with:
  - sourceUri as a mandatory string
  - targetUri as a mandatory string
- Created GetResourcesEdgesResponse in Discovery with edges as an optional array of Edges (why optional? MUST you get an empty GetResourcesEdgesResponse when edges are irrelevant?)
- includeEdges as a boolean to GetResouces with a default value of false

## Dev 13 [2020-07-08]
### Changed
- In ChannelMetadataRecord made indexes mandatory
- Made Store.PutDataObjects multipart
- Made StoreNotification.ObjectChanged multipart
- Made ChannelDataLoad.TruncateChannelsResponse multipart
- Made ChannelDataLoad.ChannelClosed multipart
- Made ChannelSubscribe.GetChangeAnnotationsResponse multipart
- Made DataArray.PutDataArraysResponse multipart
- Made DataArray.PutDataSubarraysResponse multipart
- Made DataArray.PutUninitializedDataArraysResponse multipart
- Made Dataspace.PutDataspacesResponse multipart
- Made Dataspace.DeleteDataspacesResponse multipart
- Made GrowingObject.PutGrowingDataObjectsHeaderResponse multipart
- Made GrowingObject.PutPartsResponse multipart
- Made GrowingObject.DeletePartsResponse multipart
- Made GrowingObjectNotification.SubscribePartNotificationsResponse multipart
- Made Store.PutDataObjectsResponse multipart
- Made Store.DeleteDataObjectsResponse multipart
- Made StoreNotification.SubscribeNotificationsResponse multipart
- Renamed FindObjects to FindDataObjects and renamed .avsc file
- Renamed FindObjectsResponse to FindDataObjectsResponse and renamed .avsc file
- Renamed CloseChannel to CloseChannels and renamed .avsc file
- Renamed ChannelClosed to ChannelsClosed and renamed .avsc file
### Removed
- In ChannelMetadataRecord removed default of EmptyArray
- Extra spaces from the ends of line that had them

## Dev 12 [2020-06-11]
### Added
- FrameChangeDetectionPeriod to ProtocolCapabilityKind as a long
- ArrayofBooleanNullable, ArrayOfIntNullable, and ArrayOfLongNullable in Datatype
    - notice that array types are called bags...
    - no nullable array of double, float, string
    - not added to DataValue, AnyArray or AnyArrayType (DIDNT MAKE THEM NULLABLE YET)
- AnySubarray
- AnySparseArray
- ChannelChangeAnnotation in ChannelData
- ChannelChangeRequestInfo [not ChannelChangesRequestInfo with a plural]
- ChannelChangeResponseInfo [not ChannelChangesResponseInfo with a plural] (MAP OF ARRAYS?)
- DataValueType
- SupportedDataObject
- MessageHeaderExtension
- storeLastWrite to Resource
- arrayOfBooleanNullable, arrayOfIntNullable, arrayOfLongNullable, and anySparseArray to DataValue
- arrayOfBooleanNullable, arrayOfIntNullable, arrayOfLongNullable, and anySparseArray to the DataValueType enum
- GetChangeAnnotations
- GetChangeAnnotationsResponse
- TruncateChannels
- RollbackTransactionResponse
- currentDateTime above endpointCapabilities in Core.RequestSession
- currentDateTime in Core.OpenSession
- channelChangeTime as a map of longs in ChannelDataLoad.ReplaceRangeResponse
- changeTimestamp to ChannelSubscribe.RangeReplaced above data
- success to ChannelSubscribe.SubscribeChannelsResponse
- context, scope, storeLastWriteFilter, and activeStatusFilter in DiscoveryQuery.FindResources
- context, scope, storeLastWriteFilter, and activeStatusFilter in StoreQuery.FindObjects [not FindResources]	
- dataspaceUris to StartTransaction with a default of EmptyArray [not an array with a single empty string]
- successful in Transaction.StartTransactionResponse
- failureReason in Transaction.StartTransactionResponse
### Changed
- ChannelMetaDataRecord.dataType to DataValueType
- FrameChannelMetaDataRecord.dataType to DataValueType
- AttributeMetaDataRecord.dataType to DataValueType
- ServerCapabilities.supportedObjects to supportedDataObjects
- ServerCapabilities.supportedEncodings to an array of strings with a default of binary (HOW TO SAY A DEFAULT FOR AN ARRAY???)
- GetResources.lastChangedFilter to storeLastWriteFilter
- supportedObjects to supportedDataObjects in Core.RequestSession
- supportedDataObjects to a type of SupportedDataObject in Core.RequestSession
- supportedObjects to supportedDataObjects in Core.OpenSession
- supportedDataObjects to a type of SupportedDataObject in Core.OpenSession
- supportedCompression to a plain optional string in Core.OpenSession
- SenderRole to store in ChannelDataLoad.ReplaceRangeResponse
- TruncateChannelsResponse.channelsTruncated to a map of long
### Deleted
- uri in DiscoveryQuery.FindResources
- uri in StoreQuery.FindObjects [not FindResources]


