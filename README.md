#Energistics Transfer Protocol

The Energistics Transfer Protocol (ETP) is a specification for an asynchronous message-based data transfer standard used by the Energistics family data formats. This project contains the following artifacts:

1. The UML models for the specification in XML form as well as a Sparx EA model.
2. Generated documentation for these models.
3. Generated schemas files (.avsc and/or .xsd) for protocol messages.
4. DevKits, with source code in a variety of programming languages to aid in developing clients and servers for ETP.
5. Sample implementations of simple clients and servers in a variety of programming languages.

Refer to online documentation at http://docs.energistics.org for more information.

The latest ETP 1.2 schemas are in the etp1.2 branch.